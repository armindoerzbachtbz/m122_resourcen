Bash Tutorial
===

Das Tutorial besteht aus einem Video und Übungsaufgaben, die du zu jedem Thema machen kannst.

Das Video solltest du jeweils an vorgesehenen Stellen unterbrechen und die Übungen dazu machen.

# Video

[Tutorial Video](https://web.microsoftstream.com/video/1552c2f1-fa6f-4dc4-bba0-1c185460a7f7) 20:56

# Übungen

## Vorbereitung

- Loggt auf eurer TBZ-Cloud-VM ein und führt folgende Kommandos aus:
    ```
    mkdir -p bash_tutorial/aa4..dir bash_tutorial/aa5..dir/bb1.dir
    cd bash_tutorial
    touch aa4..dir/bb{1..3}.txt aa5..dir/ba{1..5} aa{1..3} abc.txt abr.{a..z} ansible{1,2}.yml
    ```
Jetzt solltet ihr im Directory bash_tutorial stehen und darin sollten einige Files wie im Film erzeugt worden sein.

## Kommandos ausführen

- In der TBZ-Cloud sollt ihr nun nach dem Kommando `which` suchen. Wo ist es? Welches builtin Kommando führt ihr aus um das rauszufinden?
- Jetzt setzt ihr die `$PATH`-Variable neu mit folgendem Kommando:
    ```
    PATH='/usr/bin:/bin'
    ```
  und sucht mochmals noch dem Kommando `which`. Wo findet ihr nun das which Kommando?
- Findet heraus ob es sich beim Kommando `set` um ein "shell builtin"-Kommando handelt.
- Setzt die `$PATH`-Variable auf "" und führt jetzt das Kommando `ls` aus. Wie geht das ohne Einträge in der `$PATH`-Variable?
- Setzt die `$PATH`-Variable wieder auf den Standard-Wert zurueck:
    ```
    PATH='/bin:/usr/bin'
    ```


## Parameter Übergabe

- Ihr steht jetzt im verzeichnis `bash_tutorial`, oder ihr müsst jetzt in dieses wechseln.
Versucht ein Listing der Files im Subdirectory `aa4..dir` zu machen ohne in dieses Verzeichnis zu wechseln. 
- Ihr sollt nun ein Listing von allen Files in den Verzeichnissen `aa4..dir` und `aa5..dir/bb1.dir` mit nur **einem** `ls`-Kommando machen.
- Jetzt sollt ihr ein Verzeichnis mit dem Namen `keine gute Idee` erstellen. **Achtung**: Es hat Leerzeichen im Namen. Benutzt das Kommando `mkdir`.
- Jetzt sollt ihr das Verzeichnis `keine gute Idee` wieder löschen. Benutzt das Kommando `rmdir`.

## Wildcards

- Listet alle Files auf die im aktuellen Verzeichnis sind ohne `ls` zu benutzen sondern nur mit dem Kommando `echo`.

- Listet alle Files und Directories in allen Subdirectories der ersten Stufe auf. Benutzt nur das Kommando `echo`.

- Listet alle Files und Directories auf die im Parentdirectory liegen ohne `ls` zu benutzen.

- Macht mit 'ls -ld' ein Long-Listing von allen Files und Directories die an der 3 Stelle ein Buchstaben `r` haben.

## Variablen
- Setzt eine Variable `meinevariable` mit dem Wert `aa`
- Macht mit 'ls -ld' ein Long-Listing von allen Files und Directories die mit `$meinevariable` beginnen.
- Setzt eine Variable `meinevariable` mit den Namen aller Files und Directories in aktuellen Verzeichnis und gebt diese dann mit `echo` aus.
- Macht ein neues Verzeichnis mit dem Namen `meinverzeichnis` und moved mit dem `mv`-Kommando alle Files und Directories die in `$meinevariable` gespeichert sind in dieses. 
- Moved alle files in `$meinevariable` wieder zurück. Dazu müsst ihr das Verzeichnis wechseln.

## Quoting
- Erzeugt mit dem Kommando `touch` ein File mit dem Namen `$A`.
- Loescht dieses File wieder.
- Jetzt setzt ihr die Variable `A` auf den Wert `ein ganz schlechter filename`
- Jetzt sollt ihr ein file mit dem Namen der in $A gespeichert ist erstellen. **Achtung**: Leerzeichen im Namen.
- Jetzt sollt ihr dieses File wieder löschen.
- Tippe genau 8 Zeichen und die `Enter`-Taste um 4 Files `ein`, `ganz`, `schlechter` und `filename` zu erstellen.
- Tippe genau 5 zeichen und die `Enter`-Taste um diese 4 Files wieder zu löschen. 

## Aliases

- Setze folgende aliases:
  ```
  alias la='ll -a'
  alias ll='ls -l'
  ```
  Was kriegst du als output wenn du folgendes eingibst `ll meinverzeichnis` und was wenn du `la meinverzeichnis` eingibst.

- Loesche nun den alias `ll`. Funktioniert der Alias `la` noch?


## Reihenfolge

- Setze den alias `doit`:
  ```
  alias doit='$com $pat'
  ```
  Was musst du tun, damit das Kommando `doit` alle Files die mit `a` beginnen im aktuellen Verzeichnis ausgibt?




---

> [⇧ **Zurück zur Hauptseite**](../../../README.md)<br>
> [⇧ **Zurück zu Linux**](../../README.md)

---