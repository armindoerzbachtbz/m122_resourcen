# Environment - Übung

Führe die folgenden Kommandos aus:
```
unexportierte_variable="diese Variable ist nicht exportiert"
export exportierte_variable="diese Variable ist exportiert"
```
Jetzt kannst du beide Variablen ausgeben und die Ausgabe notieren:
```
echo $unexportierte_variable
echo $exportierte_variable
```
Jetzt starten wir einen Subprozess, nämlich eine bash. Es könnte aber auch ein anderes Programm sein:
```
bash
```
Jetzt kannst du nochmals beide Variablen ausgeben und die Ausgabe notieren:
```
echo $unexportierte_variable
echo $exportierte_variable
```
Was ist der Unterschied?

Jetzt verlässt du wieder den Subprozess:
```
exit
```
Jetzt kannst du nochmals beide Variablen ausgeben und die Ausgabe notieren:
```
echo $unexportierte_variable
echo $exportierte_variable
```
Fragen?
